﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetTdd
{
    public class Program
    {
        public static bool Maj(string phrase)
        {
            char Debut_Majuscule = 'A';
            char Debut_Minuscule = 'a';
            if (phrase[0] >= Debut_Majuscule && phrase[0] <= Debut_Minuscule)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Point(string phrase)
        {
            char point = '.';
            int length = phrase.Length - 1;

            if (phrase[length] == point)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static void Main(string[] args)
        {
            string phrase;

            Console.WriteLine("Saisissez votre phrase : ");
            phrase = string.Format(Console.ReadLine());

            Console.WriteLine("La phrase que vous avez saisi était : {0}", phrase);

            bool resultat_maj = Program.Maj(phrase);
            if (resultat_maj == true)
            {
                Console.WriteLine("La phrase commence par une majuscule");
            }
            else
            {
                Console.WriteLine("La phrase ne commence pas par une majuscule");
            }

            bool resultat_point = Program.Point(phrase);
            if (resultat_point == true)
            {
                Console.WriteLine("La phrase fini par un point");
            }
            else
            {
                Console.WriteLine("La phrase ne fini pas par un point");
            }

            Console.ReadKey();
        }
    }
}