﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using ProjetTdd;

namespace ProjetTddTest
{
    [TestClass]
    public class TestMajEtPoint
    {
        [TestMethod]
        public void Verifmaj()
        {
            Assert.AreEqual(true, Program.Maj("Debut par une majuscule"));
            Assert.AreEqual(false, Program.Maj("debut par une minuscule"));
        }

        [TestMethod]
        public void Verifpoint()
        {
            Assert.AreEqual(true, Program.Point("fin avec point."));
            Assert.AreEqual(false, Program.Point("fin sans point"));
        }
    }
}
